import setuptools

setuptools.setup(
    name="polytracker", # Replace with your own username
    version="0.0.1",
    author="Jonathan",
    author_email="jonathan@pocydon.com",
    description="Fuzzing",
    long_description="PolyTracker is a tool for the Automated Lexical Annotation and Navigation of Parsers, a backronym devised solely for the purpose of referring to it as The ALAN Parsers Project. It is a an LLVM pass that instruments the programs it compiles to track which bytes of an input file are operated on by which functions.",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/twosix-kali/packages1/polytracker",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)

